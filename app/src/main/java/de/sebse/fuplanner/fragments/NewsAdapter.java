package de.sebse.fuplanner.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.fragments.ModulesFragment.OnModulesFragmentInteractionListener;
import de.sebse.fuplanner.services.kvv.types.Modules;
import de.sebse.fuplanner.tools.UtilsDate;
import de.sebse.fuplanner.tools.types.News;
import de.sebse.fuplanner.tools.types.NewsList;
import de.sebse.fuplanner.tools.ui.NewsViewHolder;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Modules.Module} and makes a call to the
 * specified {@link OnModulesFragmentInteractionListener}.
 */
class NewsAdapter extends RecyclerView.Adapter<NewsViewHolder> {


    private NewsList mValues;

    NewsAdapter() {
        mValues = null;
    }

    public void setNews(NewsList news) {
        mValues = news;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_news_item, parent, false);
            return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        News news = mValues.getPast(position);
        holder.mHeader.setText(news.getTitle());
        switch (news.getCategory()) {
            case News.CATEGORY_UPDATE:
                holder.mSubLeft.setText(R.string.update_news);
            case News.CATEGORY_TRICKS:
                holder.mSubLeft.setText(R.string.tricks);
        }
        holder.mSubRight.setText(UtilsDate.getModifiedDate(news.getDate()));
        holder.mText.setText(news.getText());
    }

    @Override
    public int getItemCount() {
        return mValues != null ? mValues.sizePast() : 0;
    }
}
