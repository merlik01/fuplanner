package de.sebse.fuplanner.fragments.canteen;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import de.sebse.fuplanner.services.canteen.types.Canteen;
import de.sebse.fuplanner.tools.UtilsDate;

class DaySwitcherAdapter extends FragmentStatePagerAdapter {
    private Canteen mCanteen = null;

    DaySwitcherAdapter(FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    public void setCanteen(Canteen canteen) {
        mCanteen = canteen;
        this.setCanteen();
    }

    public void setCanteen() {
        this.notifyDataSetChanged();
    }


    // Returns total number of pages
    @Override
    public int getCount() {
        return mCanteen == null ? 0 : mCanteen.size();
    }

    // Returns the fragment to display for that page
    @NonNull
    @Override
    public Fragment getItem(int position) {
        return MealFragment.newInstance(mCanteen.getId(), position);
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return UtilsDate.getModifiedDate(mCanteen.get(position).getCalendar().getTimeInMillis());
    }

}
