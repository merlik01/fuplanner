package de.sebse.fuplanner.fragments.moddetails;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.kvv.types.Modules;
import de.sebse.fuplanner.tools.MainActivityListener;
import de.sebse.fuplanner.tools.logging.Logger;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainActivityListener} interface
 * to handle interaction events.
 * Use the {@link ModDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ModDetailFragment extends Fragment implements ModDetailListener {
    private static final String ARG_POSITION = "itemPosition";

    // Parameters
    private String mItemPos;

    private MainActivityListener mListener;
    private final Logger log = new Logger(this);
    private ViewPager mViewPager;
    private String mPageRestoreRequest = null;

    public ModDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param itemPosition Item position in module list.
     * @return A new instance of fragment ModDetailFragment.
     */
    public static Fragment newInstance(String itemPosition) {
        ModDetailFragment fragment = new ModDetailFragment();
        Bundle args = new Bundle();
        if (!itemPosition.contains("."))
            args.putString(ARG_POSITION, itemPosition+".0");
        else
            args.putString(ARG_POSITION, itemPosition);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String itemPosition = getArguments().getString(ARG_POSITION);
            if (!itemPosition.contains(".")) {
                mItemPos = itemPosition;
                mPageRestoreRequest = null;
            } else {
                String[] split = itemPosition.split("\\.", 2);
                mItemPos = split[0];
                mPageRestoreRequest = split[1];
            }
        }
        if (mListener != null) {
            mListener.onTitleTextChange(R.string.courses);
            mListener.getKVV(kvv -> {
                kvv.modules().list().recv(success -> {
                    Modules.Module module = success.get(mItemPos);
                    if (mListener != null && module != null)
                        mListener.onTitleTextChange(module.title);
                }, log::e);
            });
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_mod_detail, container, false);

        mViewPager = v.findViewById(R.id.vpPager);
        ModDetailAdapter adapterViewPager = new ModDetailAdapter(getChildFragmentManager(), mItemPos, getContext());
        mViewPager.setAdapter(adapterViewPager);
        if (mPageRestoreRequest != null)
            mViewPager.setCurrentItem(Integer.parseInt(mPageRestoreRequest));
        return v;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivityListener) {
            mListener = (MainActivityListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void gotoFragmentPart(int part, int index) {
        mViewPager.setCurrentItem(ModulePart.getPageByPart(part), true);
    }

    public String getData() {
        return mItemPos+"."+mViewPager.getCurrentItem();
    }
}
