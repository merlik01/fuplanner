package de.sebse.fuplanner.services.canteen.types;

import de.sebse.fuplanner.tools.SortedList;

public class SortedListMeal extends SortedList<Meal, Integer, String> {
    @Override
    public int compare(Meal o1, Meal o2) {
        return Integer.compare(o1.getId(), o2.getId());
    }

    @Override
    public boolean hasIdentifier(Meal o1, Integer id) {
        return o1.getId() == id;
    }

    @Override
    public boolean hasFilter(Meal o1, String filter) {
        return o1.getCategory().equals(filter);
    }
}
