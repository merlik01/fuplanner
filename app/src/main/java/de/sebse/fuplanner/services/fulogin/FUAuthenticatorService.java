package de.sebse.fuplanner.services.fulogin;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class FUAuthenticatorService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        FUAuthenticator authenticator = new FUAuthenticator(this);
        return authenticator.getIBinder();
    }
}
