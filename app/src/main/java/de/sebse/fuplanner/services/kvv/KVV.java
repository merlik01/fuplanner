package de.sebse.fuplanner.services.kvv;

import android.accounts.AccountManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.android.volley.NetworkResponse;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import de.sebse.fuplanner.services.kvv.types.LoginTokenBB;
import de.sebse.fuplanner.services.kvv.types.LoginTokenKVV;
import de.sebse.fuplanner.tools.CustomAccountManager;

public class KVV extends Service {
    private final HashMap<String, Object> addons = new HashMap<>();
    private final HashMap<String, KVVListener> mListeners = new HashMap<>();
    private final KVVListener mListener = new KVVListener() {
        CustomAccountManager accountManager = null;

        @Override
        public CustomAccountManager getAccountManager() {
            if (accountManager == null)
                accountManager = new CustomAccountManager(AccountManager.get(KVV.this.getApplicationContext()), () -> null);
            return accountManager;
        }

        @Override
        public void onKVVNetworkResponse(NetworkResponse error) {
            for (KVVListener listener : mListeners.values())
                listener.onKVVNetworkResponse(error);
        }

        @Override
        public void onLogin(LoginTokenKVV tokenKVV, LoginTokenBB tokenBB, boolean isOnlyRefresh) {
            for (KVVListener listener : mListeners.values())
                listener.onLogin(tokenKVV, tokenBB, isOnlyRefresh);
        }

        @Override
        public void onLogout() {
            for (KVVListener listener : mListeners.values())
                listener.onLogout();
        }

        @Override
        public void onModuleListChange() {
            for (KVVListener listener : mListeners.values())
                listener.onModuleListChange();
        }

        @Override
        public void onModuleListPartiallyUpdated(de.sebse.fuplanner.services.kvv.types.Modules modules) {
            for (KVVListener listener : mListeners.values())
                listener.onModuleListPartiallyUpdated(modules);
        }
    };

    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();

    @NotNull
    public Login account() {
        return (Login) addAndGet("account", () -> new Login(mListener, getApplicationContext()));
    }

    @NotNull
    public Modules modules() {
        return (Modules) addAndGet("module", () -> new Modules(account(), mListener, getApplicationContext()));
    }

    public void addListener(String key, KVVListener listener) {
        mListeners.put(key, listener);
    }

    public void removeListener(String key) {
        mListeners.remove(key);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public KVV getService() {
            // Return this instance of LocalService so clients can call public methods
            return KVV.this;
        }
    }







    @NotNull
    private Object addAndGet(@NotNull String addon, @NotNull ModuleCreatorInterface creatorInterface) {
        Object o = addons.get(addon);
        if (o == null) {
            o = creatorInterface.create();
            addons.put(addon, o);
        }
        return o;
    }

    private interface ModuleCreatorInterface {
        @NotNull Object create();
    }
}
