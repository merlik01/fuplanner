package de.sebse.fuplanner.services.kvv;

import android.content.Context;

import java.io.IOException;

import de.sebse.fuplanner.services.kvv.types.CacheBBEventNumber;
import de.sebse.fuplanner.tools.Regex;
import de.sebse.fuplanner.tools.network.HTTPService;
import de.sebse.fuplanner.tools.network.NetworkCallback;
import de.sebse.fuplanner.tools.network.NetworkError;
import de.sebse.fuplanner.tools.network.NetworkErrorCallback;

class ModulesEventsNumber extends HTTPService {
    private final CacheBBEventNumber mStorage;

    ModulesEventsNumber(Context context) {
        super(context);
        CacheBBEventNumber storage = null;
        try {
            storage = CacheBBEventNumber.load(context);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (storage == null) {
            mStorage = new CacheBBEventNumber();
        } else {
            mStorage = storage;
        }
    }

    void getVVNumber(String lvNumber, NetworkCallback<String> callback, NetworkErrorCallback errorCallback) {
        String vvNumber = mStorage.getVVNumber(lvNumber);
        if (vvNumber != null) {
            callback.onResponse(vvNumber);
            return;
        }

        super.head(String.format("https://www.fu-berlin.de/vv/de/search?utf8=✓&query=%s", lvNumber), null, response -> {
            String location = response.getHeaders().get("Location");
            if (location == null) {
                // Events not available
                callback.onResponse("");
                //errorCallback.onError(new NetworkError(101410, 403, "Cannot get events!"));
                return;
            }
            try {
                String group = Regex.regex("lv/([0-9]+)\\?", location);
                mStorage.setVVNumber(lvNumber, group);
                try {
                    mStorage.save(getContext());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                callback.onResponse(group);
            } catch (NoSuchFieldException e) {
                errorCallback.onError(new NetworkError(102201, 400, "Cannot get events!"));
                e.printStackTrace();
            }
        }, error -> errorCallback.onError(new NetworkError(102202, error.networkResponse.statusCode, "Error retrieving lecturer!")));
    }
}
