package de.sebse.fuplanner.services.kvv.types;

import com.google.android.gms.common.internal.Objects;

import java.io.Serializable;
import java.util.ArrayList;

import androidx.annotation.NonNull;

public class Assignment implements Serializable {
    private final String id;
    private final String title;
    private final long dueTime;
    private final ArrayList<String> urls;
    private final String instructions;

    public Assignment(String id, String title, long dueTime, String gradebookItemName, String gradeScale, ArrayList<String> urls, String instructions) {//, String grade
        this.id = id;
        this.title = title;
        this.dueTime = dueTime;
        this.urls = urls;
        this.instructions = instructions;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public boolean isOpen() {
        return dueTime > System.currentTimeMillis();
    }

    public long getDueDate() {
        return dueTime;
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    public String getInstructions() {
        return instructions;
    }

    @NonNull
    @Override
    public String toString() {
        return "ID: "+getId()+
                "\nTitle: "+getTitle()+
                "\nDue date: "+getDueDate()+
                "\nInstructions: "+getInstructions().substring(0, Math.min(getInstructions().length(), 100));
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId(), getDueDate(), getInstructions(), getTitle(), getUrls());
    }
}
