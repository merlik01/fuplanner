package de.sebse.fuplanner.services.kvv.types;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;

public class CacheBBEventNumber implements Serializable {
    private transient static final long RESAVE_TIMER = 1000L * 60 * 60 * 24 * 30;
    private static final String FILE_NAME = "BBEventNumberStorageSaving";
    private static final String FILE_NAME_TIMESTAMP = "BBEventNumberStorageSavingTimestamp";
    private transient long mLastTimestamp = 0;

    private HashMap<String, String> mBBEventNumbers = new HashMap<>();
    private HashMap<String, Long> mBBEventNumbersRefresh = new HashMap<>();

    public static CacheBBEventNumber load(Context context) throws IOException, ClassNotFoundException {
        FileInputStream fis = context.openFileInput(FILE_NAME);
        ObjectInputStream is = new ObjectInputStream(fis);
        Object readObject = is.readObject();
        if (!(readObject instanceof CacheBBEventNumber))
            return null;
        CacheBBEventNumber storage = (CacheBBEventNumber) readObject;
        is.close();
        fis.close();

        fis = context.openFileInput(FILE_NAME_TIMESTAMP);
        is = new ObjectInputStream(fis);
        storage.mLastTimestamp = is.readLong();
        is.close();
        fis.close();

        return storage;
    }

    public boolean isNewerVersionInStorage(Context context) throws IOException {
        FileInputStream fis = context.openFileInput(FILE_NAME_TIMESTAMP);
        ObjectInputStream is = new ObjectInputStream(fis);
        boolean result = this.mLastTimestamp < is.readLong();
        is.close();
        fis.close();
        return result;
    }

    public void save(Context context) throws IOException {
        FileOutputStream fos = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(this);
        os.close();
        fos.close();

        fos = context.openFileOutput(FILE_NAME_TIMESTAMP, Context.MODE_PRIVATE);
        os = new ObjectOutputStream(fos);
        this.mLastTimestamp = System.currentTimeMillis();
        os.writeLong(this.mLastTimestamp);
        os.close();
        fos.close();
    }

    public void setVVNumber(String lvNumber, String vvNumber) {
        mBBEventNumbers.put(lvNumber, vvNumber);
        mBBEventNumbersRefresh.put(lvNumber, System.currentTimeMillis());
    }

    public String getVVNumber(String lvNumber) {
        if (!mBBEventNumbersRefresh.containsKey(lvNumber))
            return null;
        //noinspection ConstantConditions
        if (mBBEventNumbersRefresh.get(lvNumber) + RESAVE_TIMER < System.currentTimeMillis())
            return null;
        return mBBEventNumbers.get(lvNumber);
    }
}
