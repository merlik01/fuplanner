package de.sebse.fuplanner.services.kvv.types;

import com.google.android.gms.common.internal.Objects;

import java.io.Serializable;

import androidx.annotation.NonNull;

public class Grade implements Serializable {
    private final String itemName;
    private final double grade;
    private final double maxPoints;

    public Grade(String itemName, double points, double maxPoints) {
        this.itemName = itemName;
        this.grade = points;
        this.maxPoints = maxPoints;
    }

    public double getMaxPoints() {
        return maxPoints;
    }

    public double getPoints() {
        return grade;
    }

    public String getItemName() {
        return itemName;
    }

    @NonNull
    @Override
    public String toString() {
        return "Name: "+getItemName()+
                "\nPoints: "+ getPoints()+
                "\nMax points: "+getMaxPoints();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getItemName(), getPoints(), getMaxPoints());
    }
}
