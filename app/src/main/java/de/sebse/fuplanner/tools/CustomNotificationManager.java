package de.sebse.fuplanner.tools;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import org.jetbrains.annotations.NotNull;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import de.sebse.fuplanner.MainActivity;
import de.sebse.fuplanner.R;

public class CustomNotificationManager {
    private static final String CHANNEL_ID = "fuplanner-default";
    public static final String NOTIFICATION_INTENT = "notification-intent";
    public static final String NOTIFICATION_PAGE = "notification-page";
    public static final String NOTIFICATION_DATA = "notification-data";
    public static final String NOTIFICATION_TYPE_NAVIGATE = "navigate";

    //public static ArrayList<Integer> passedNotifications = new ArrayList<>();

    public static void sendNotification(Context context, String textTitle, String textContent, int page, String data) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(NOTIFICATION_INTENT, NOTIFICATION_TYPE_NAVIGATE);
        intent.putExtra(NOTIFICATION_PAGE, page);
        intent.putExtra(NOTIFICATION_DATA, data);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(textTitle)
                .setContentText(textContent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        int notificationId = (int) (System.currentTimeMillis() % Integer.MAX_VALUE);
        notificationManager.notify(notificationId, mBuilder.build());
    }

    public static void createNotificationChannel(@NotNull Context context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.channel_name);
            String description = context.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }
}
