package de.sebse.fuplanner.tools;

import androidx.annotation.StringRes;

import de.sebse.fuplanner.MainActivity;
import de.sebse.fuplanner.services.canteen.CanteenBrowser;
import de.sebse.fuplanner.services.news.NewsManager;

public interface MainActivityListener {
    void onTitleTextChange(String newTitle);

    void onTitleTextChange(@StringRes int titleId);

    void showToast(String message);

    void showToast(@StringRes int msgStringRes);

    void getKVV(MainActivity.KVVCallback kvv);

    CanteenBrowser getCanteenBrowser();

    NewsManager getNewsManager();

    void addRequestPermissionsResultListener(RequestPermissionsResultListener listener, String id);

    void removeRequestPermissionsResultListener(String id);
}
