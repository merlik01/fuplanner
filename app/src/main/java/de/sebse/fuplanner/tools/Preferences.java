package de.sebse.fuplanner.tools;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.ArrayRes;
import androidx.annotation.StringRes;
import androidx.preference.PreferenceManager;

public class Preferences {
    public static String getStringArray(Context context, @ArrayRes int key) {
        String[] strings = context.getResources().getStringArray(key);
        return PreferenceManager.getDefaultSharedPreferences(context).getString(strings[0], strings[1]);
    }


    public static String getString(Context context, @StringRes int key) {
        String string = context.getResources().getString(key);
        return PreferenceManager.getDefaultSharedPreferences(context).getString(string, null);
    }
    public static void setString(Context context, @StringRes int key, String value) {
        String string = context.getResources().getString(key);
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(string, value).apply();
    }

    public static long getLong(Context context, @StringRes int key) {
        String string = context.getResources().getString(key);
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(string, 0);
    }
    public static void setLong(Context context, @StringRes int key, long value) {
        String string = context.getResources().getString(key);
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(string, value).apply();
    }

    public static boolean getBoolean(Context context, @StringRes int key) {
        String string = context.getResources().getString(key);
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(string, false);
    }
    public static void setBoolean(Context context, @StringRes int key, boolean value) {
        String string = context.getResources().getString(key);
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(string, value).apply();
    }

    public static int[] getArrayInt(Context context, @StringRes int key) {
        String string = context.getResources().getString(key);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (!prefs.contains(string + "_size")) {
            return null;
        }
        int size = prefs.getInt(string + "_size", 0);
        int[] array = new int[size];
        for (int i = 0; i < size; i++)
            array[i] = prefs.getInt(string + "_" + i, 0);
        return array;
    }
    public static void setArrayInt(Context context, @StringRes int key, int[] value) {
        String string = context.getResources().getString(key);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        int oldSize = prefs.getInt(string + "_size", 0);
        int newSize = 0;
        if (value == null) {
            editor.remove(string + "_size");
        } else {
            newSize = value.length;
            editor.putInt(string +"_size", newSize);
        }
        for (int i = 0; i < newSize; i++)
            editor.putInt(string + "_" + i, value[i]);
        for (int i = newSize; i < oldSize; i++)
            editor.remove(string + "_" + i);
        editor.apply();
    }
}
