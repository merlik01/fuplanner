package de.sebse.fuplanner.tools.network;

import androidx.annotation.NonNull;

public interface NetworkCallback<T> {
    void onResponse(@NonNull T success);
}

