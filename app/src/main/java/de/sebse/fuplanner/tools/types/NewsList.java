package de.sebse.fuplanner.tools.types;

import de.sebse.fuplanner.tools.DateSortedList;

public class NewsList extends DateSortedList<News> {
    @Override
    protected long getDateByItem(News item) {
        return item.getDate();
    }

    @Override
    protected boolean reversed() {
        return true;
    }
}
