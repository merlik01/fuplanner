package de.sebse.fuplanner.tools.ui;

import android.view.View;
import android.widget.ImageView;

import de.sebse.fuplanner.R;

public class CanteensViewHolder extends ItemViewHolder {
    public final ImageView mActionIcon;

    public CanteensViewHolder(View view) {
        super(view);
        mActionIcon = view.findViewById(R.id.remove_icon);
    }
}
