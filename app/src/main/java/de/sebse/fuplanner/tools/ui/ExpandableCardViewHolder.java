package de.sebse.fuplanner.tools.ui;

import android.view.View;

import de.sebse.fuplanner.tools.ui.cardview.ExpandableCardView;

public class ExpandableCardViewHolder extends CustomViewHolder {
    ExpandableCardViewHolder(View view) {
        super(view);
    }

    public void reset() {
        getView().reset();
    }

    private ExpandableCardView getView() {
        return (ExpandableCardView) mView;
    }

    View getOuterView() {
        return getView().getOuterView();
    }

    View getInnerView() {
        return getView().getInnerView();
    }
}
