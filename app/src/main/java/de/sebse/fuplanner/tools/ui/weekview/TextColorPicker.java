package de.sebse.fuplanner.tools.ui.weekview;

import androidx.annotation.ColorInt;



public interface TextColorPicker {

    @ColorInt
    int getTextColor(WeekViewEvent event);

}
